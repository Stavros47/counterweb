package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CntTest {
        @Test
        public void testBZero() throws Exception {
                int res = new Cnt().d(10,0);
                assertEquals("result",res,Integer.MAX_VALUE);
        }

	@Test
	public void testB() throws Exception {
		int res = new Cnt().d(10,2);
		assertEquals("result", res, 5);
	}
}

